const nodemailer = require('nodemailer');
const mustache = require('mustache');
const fs = require('fs');
require('dotenv').config();

const clientId = '699950261067-3po57i42aaov8iab84vq6va4ollefasn.apps.googleusercontent.com';
const clientSecret = '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';
const refreshToken =
  '1//04qNu8e6-skvQCgYIARAAGAQSNwF-L9IrzlsjfYbTPYgAcd8YrUL3Y5JMlkKrJmS4TWJZRjQsly7y-ZR3pU4W3eGhBX8Pvq4hg9k';

const { google } = require('googleapis');
const OAuth2 = google.auth.OAuth2;
const OAuth2_client = new OAuth2(clientId, clientSecret);
OAuth2_client.setCredentials({
  refresh_token: refreshToken,
});

const sendMail = (data) =>
  new Promise((resolve, reject) => {
    const { to, subject, template } = data;

    const fileTemplate = fs.readFileSync(`src/templates/email/${template}`, 'utf8');

    const accessToken = OAuth2_client.getAccessToken;

    const transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        type: 'OAuth2',
        user: 'memo.in.aja@gmail.com',
        clientId: clientId,
        clientSecret: clientSecret,
        refreshToken: refreshToken,
        accessToken: accessToken,
      },
    });

    const mailOptions = {
      from: '"ZWallet 👻" <memo.in.aja@gmail.com>', // sender address
      to: to, // list of receivers
      subject: subject, // Subject line
      html: mustache.render(fileTemplate, { ...data }),
    };
    transporter.sendMail(mailOptions, async (error, info) => {
      if (error) {
        reject(error);
      } else {
        resolve(info.response);
      }
    });
  });

module.exports = sendMail;
